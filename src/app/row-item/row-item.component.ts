import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../user.model';

@Component({
  selector: '[app-row-item]',
  templateUrl: './row-item.component.html',
  styleUrls: ['./row-item.component.css']
})
export class RowItemComponent implements OnInit {
  @Input() user!: User;
  @Output() edited = new EventEmitter();

  private isEditing: boolean = false;
  form: FormGroup;

  get editing(): boolean {
    return this.isEditing;
  }

  get reading(): boolean  {
    return this.isEditing == false;
  }
  constructor() {
    this.form = new FormGroup({
      pseudo: new FormControl('', Validators.required),
      firstname: new FormControl(),
      lastname: new FormControl(),
    });

    // Keep corresponding controls in sync
    for (const controlName in this.form.controls) {
      const control = this.form.get(controlName)!;

      control.valueChanges.subscribe(value => {
          control.setValue(value, { onlySelf: true, emitEvent: false}); // options avoids infinite loop
      });
    }
  }

  ngOnInit(): void {
  }

  edit() {
    this.isEditing = true;

    const {id, ...formValues} = this.user;
    this.form.setValue(formValues);
  }

  save() {
    this.isEditing = false;

    Object.assign(this.user, this.form.value);
    this.edited.emit(); // emit only if value changed

    console.log(this.form.value);
    console.log(this.user);
  }
}
