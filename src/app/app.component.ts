import { Component } from '@angular/core';
import { User } from './user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  users = [
    new User({ id: 1, pseudo: 'j_doe', firstname: 'John', lastname: 'Doe'}),
    new User({ id: 2, pseudo: '__mike__', firstname: 'Magic', lastname: 'Mike'})
    ,
    new User({ id: 3, pseudo: 'THE_GOAT', firstname: 'Chuck', lastname: 'Norris'})
  ]

  sortColumn: string = 'pseudo';
  sortOrder: 1 | -1 = 1;

  getSortDisplay(column: string): string {
    if (this.sortColumn == column) {
      if (this.sortOrder > 0) {
        return '🔼';
      } else {
        return '🔽';
      }
    } else {
      return '';
    }
  }

  constructor() {
    this.sort();
  }

  updateSort(column: string) {
    if (column == this.sortColumn) {
      this.sortOrder *= -1;
    } else {
      this.sortColumn = column;
      this.sortOrder = 1;
    }

    this.sort()
  }

  sort() {
    this.users.sort((left: any, right: any) => {
      const leftLower = left[this.sortColumn].toLowerCase();
      const rightLower =  right[this.sortColumn].toLowerCase();

      if (leftLower < rightLower) {
        return -1 * this.sortOrder;
      } else if (leftLower > rightLower) {
        return 1 * this.sortOrder;
      } else {
        return 0;
      }
    });
  }
}
